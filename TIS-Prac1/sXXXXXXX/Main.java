import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Random;

public class Main {

    
    // you only need to change the main function here.
	public static void main(String[] args) throws Exception {
        Class.forName("org.h2.Driver");

        String DBurl = "jdbc:h2:tcp://localhost:9092/~/test";
        
        if (args != null && args.length > 0) DBurl = args[0];
        
        Connection conn = DriverManager.getConnection(DBurl, "sa", "");
        Statement stmt = null;
           
        DatabaseMetaData dbmd = conn.getMetaData();
    	System.out.println("Database system: " 	+ dbmd.getDatabaseProductName());
        System.out.println( "Version        : "	+ dbmd.getDatabaseMajorVersion() + "." +
                            dbmd.getDatabaseMinorVersion());
   
    	
        // lets have a look on all tables
    	ResultSet rs = dbmd.getTables(null, null, "%", null);
    	while (rs.next()) {
            System.out.println(rs.getString(3));
        }
        stmt        = conn.createStatement();
        String sql  = "";

        try{
            // create table NEWPOPULATION
            System.out.println("Creating table in db ... ");
            String checkTable =     " select count(*) from information_schema.TABLES" +
                                    " where table_name = 'NEWPOPULATION'";
            int judgeExist = 0;
            rs  =   stmt.executeQuery(checkTable);
            while(rs.next()){
                judgeExist = rs.getInt(1);
            }
            if(judgeExist == 0){
                sql =    " CREATE TABLE NEWPOPULATION " +
                                " (COUNTRY_CODE VARCHAR(4), " +
                                " AMOUNT INT)";
                stmt.executeUpdate(sql);
                System.out.println("Created table in db ... ");
            }
        }catch(Exception e){
            System.err.println(e);
        }

        try{
            // Add column CURRENT_POPULATION (int) to country
            rs  =   stmt.executeQuery(  " select count(*) from " + 
                                        " information_schema.columns" + 
                                        " where table_name = 'COUNTRY' and column_name = 'CURRENT_POPULATION'");
            int judgeExist = 0;
            while(rs.next()){
                judgeExist = rs.getInt(1);
            }
            if(judgeExist == 0){
                sql =   " ALTER TABLE country " +
                        " ADD CURRENT_POPULATION int";
                stmt.executeUpdate(sql);
                System.out.println("Insert column in table ... ");
            }
        }catch(Exception e){
            System.err.println(e);
        }   
        
        try{
            // Insert 100 tuples into NEWPOPULATION
            sql =   " SELECT CODE FROM COUNTRY " + 
                    " LIMIT 100 ";
            rs = stmt.executeQuery(sql);
            while(rs.next()){
                Random random = new Random();
                String code = rs.getString("code");
                String num  = Integer.toString(random.nextInt(100));
                sql =   " INSERT INTO NEWPOPULATION " +
                        " (COUNTRY_CODE, AMOUNT) " + 
                        " VALUES ( " + "\'" + code + "\', " + num + ")" ;
                conn.createStatement().executeUpdate(sql);
            }
            System.out.println("Insert tuple in table ... ");

            // Update COUNTRY.CURRENT_POPULATION with the new values
            sql =   " UPDATE COUNTRY " +
                    " SET CURRENT_POPULATION = POPULATION + (" + 
                    " SELECT SUM(AMOUNT) FROM NEWPOPULATION " +
                    " WHERE (COUNTRY.CODE = NEWPOPULATION.COUNTRY_CODE))";
            stmt.executeUpdate(sql);
            System.out.println("Update COUNTRY.CURRENT_POPULATION with the new values finished!");

        }catch(Exception e){
            System.err.println(e);
        }
    	
        // do something else ...
        rs.close();
        
        conn.close();
	}
	
}


