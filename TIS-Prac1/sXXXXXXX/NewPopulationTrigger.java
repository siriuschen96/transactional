import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import org.h2.api.Trigger;

public class NewPopulationTrigger implements Trigger {

    
    // you only need to change the fire function here
	@Override
	public void fire(Connection arg0, Object[] arg1, Object[] arg2)
			throws SQLException {
		PreparedStatement prep = arg0.prepareStatement(	" UPDATE COUNTRY " +
														" SET CURRENT_POPULATION = POPULATION + (" + 
														" SELECT SUM(AMOUNT) FROM NEWPOPULATION " +
														" WHERE (COUNTRY.CODE = NEWPOPULATION.COUNTRY_CODE))");
		prep.execute();
	}

	@Override
	public void init(Connection arg0, String arg1, String arg2, String arg3,
			boolean arg4, int arg5) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	

}


